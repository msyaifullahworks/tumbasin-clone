import "./App.css";
import React from "react";
//importing component
import Homepage from "./pages/homepage";

function App() {
  return (
    <div className="App">
      <Homepage />
    </div>
  );
}

export default App;
