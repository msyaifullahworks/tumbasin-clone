//imporitng component
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import StoreIcon from "@material-ui/icons/Store";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    padding: "0 16px",
  },
  appBar: {
    maxWidth: 409,
    width: "100%",
    height: 85,
    marginTop: -48,
    position: "relative",
  },
  container: {
    padding: "2px 20px",
  },
  store: {
    marginTop: "3px",
    width: 200,
    paddingLeft: "15px",
  },
  title: {
    cursor: "pointer",
    margin: "10px 0",
    paddingLeft: 20,
    fontSize: 14,
    fontFamily: "Montserrat, sans-serrif",
  },
  button: {
    marginBottom: 8,
    // marginRight: -16,
    backgroundColor: "#F15B5D",
    border: "none",
    borderRadius: "4px",
    color: "white",
    fontSize: "13px",
    height: 30,
    width: 64,
    boxShadow:
      "0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12);",
    cursor: "pointer",
    fontFamily: "Montserrat, sans-serrif",
  },
  label: {
    width: "100%",
  },
  icon: {
    color: "rgb(135, 202, 254)",
    width: 30,
    height: 30,
    marginTop: -10,
  },
  stores: {
    paddingLeft: 20,
  },
  appBarWrapper: {
    width: "100%",
    maxWidth: 409,
    display: "flex",
    justifyContent: "center",
  },
}));

function AppBar({ onTop }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.appBarWrapper}>
        <Paper className={classes.appBar}>
          <Typography className={classes.title}>Kamu Belanja Di:</Typography>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className={classes.stores}
          >
            <Grid item xs={1}>
              <StoreIcon className={classes.icon} />
            </Grid>
            <Grid item xs={8}>
              <b>
                <p className={classes.store}>Pasar Bulu Semarang</p>
              </b>
            </Grid>
            <Grid item xs={3}>
              <button className={classes.button}>GANTI</button>
            </Grid>
          </Grid>
        </Paper>
      </div>
    </div>
  );
}

export default AppBar;
