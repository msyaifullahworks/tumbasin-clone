import React from "react";
import ContentLoader from "react-content-loader";

const SkeletonCategories = () => (
  <ContentLoader
    speed={2}
    height={400}
    viewBox="0 0 444 400"
    backgroundColor="lightgrey"
    // foregroundColor="black"
    style={{ width: "100%", maxWidth: 444, marginTop: -100 }}
  >
    <rect x="20" y="100" rx="0" ry="0" width="190" height="18" />
    <rect x="20" y="140" rx="0" ry="0" width="60" height="60" />
    <rect x="106" y="140" rx="0" ry="0" width="60" height="60" />
    <rect x="192" y="140" rx="0" ry="0" width="60" height="60" />
    <rect x="278" y="140" rx="0" ry="0" width="60" height="60" />
    <rect x="364" y="140" rx="0" ry="0" width="60" height="60" />
    <rect x="20" y="210" rx="0" ry="0" width="60" height="14" />
    <rect x="106" y="210" rx="0" ry="0" width="60" height="14" />
    <rect x="192" y="210" rx="0" ry="0" width="60" height="14" />
    <rect x="278" y="210" rx="0" ry="0" width="60" height="14" />
    <rect x="364" y="210" rx="0" ry="0" width="60" height="14" />
    <rect x="20" y="240" rx="0" ry="0" width="60" height="60" />
    <rect x="106" y="240" rx="0" ry="0" width="60" height="60" />
    <rect x="192" y="240" rx="0" ry="0" width="60" height="60" />
    <rect x="278" y="240" rx="0" ry="0" width="60" height="60" />
    <rect x="364" y="240" rx="0" ry="0" width="60" height="60" />
    <rect x="20" y="310" rx="0" ry="0" width="60" height="14" />
    <rect x="106" y="310" rx="0" ry="0" width="60" height="14" />
    <rect x="192" y="310" rx="0" ry="0" width="60" height="14" />
    <rect x="278" y="310" rx="0" ry="0" width="60" height="14" />
    <rect x="364" y="310" rx="0" ry="0" width="60" height="14" />
  </ContentLoader>
);

export default SkeletonCategories;
