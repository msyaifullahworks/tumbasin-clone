import React from "react";
import ContentLoader from "react-content-loader";

const SkeletonBanner = () => (
  <ContentLoader
    speed={2}
    height={300}
    viewBox="0 0 444 300"
    backgroundColor="lightgrey"
    // foregroundColor="black"
    style={{ width: "100%", maxWidth: 444, marginTop: -100 }}
  >
    <rect x="24" y="100" rx="7" ry="7" width="400.5" height="170" />
  </ContentLoader>
);

export default SkeletonBanner;
