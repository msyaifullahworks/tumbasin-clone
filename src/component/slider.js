import React from "react";
//importing component
import { makeStyles, useTheme } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import MobileStepper from "@material-ui/core/MobileStepper";
import { autoPlay } from "react-swipeable-views-utils";
import { useQuery } from "react-query";
import SkeletonBanner from "./skeleton-banner";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const getBanner = async () => {
  const URL = `https://dashboard.tumbasin.id/wp-json/wp/v2/media?media_category[]=39`;
  const response = await fetch(URL);
  return await response.json();
};

const useStyles = makeStyles((theme) => ({
  root: {
    maxHeight: 176.39,
    maxWidth: 455,
    flexGrow: 1,
    padding: 10,
    borderRadius: 20,
    [theme.breakpoints.down("xs")]: {
      height: 144,
    },
  },
  img: {
    height: "100%",
    display: "block",
    maxWidth: 424,
    overflow: "hidden",
    width: "100%",
    borderRadius: "7px",
    [theme.breakpoints.down("xs")]: {
      height: 141.44,
    },
  },
  stepper: {
    backgroundColor: "unset",
    marginTop: -20,
    position: "absolute",
    zIndex: 9,
  },
  dotActive: {
    width: 20,
    height: 10,
    backgroundColor: "white",
    borderRadius: 6,
  },
}));

function SwipeableTextMobileStepper() {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const handleStepChange = (step) => {
    setActiveStep(step);
  };
  const { data, isLoading, isSuccess, isError } = useQuery("banner", getBanner);

  return (
    <div className={classes.root}>
      {isLoading && <SkeletonBanner />}
      {isError && <p>There was an error processing your request</p>}
      <AutoPlaySwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {isSuccess &&
          data?.map((banner, index) => (
            <div key={banner.label}>
              {Math.abs(activeStep - index) <= 2 ? (
                <img
                  className={classes.img}
                  src={banner.guid.rendered}
                  alt={banner.label}
                />
              ) : null}
            </div>
          ))}
      </AutoPlaySwipeableViews>
      <MobileStepper
        variant="dots"
        steps={3}
        position="static"
        activeStep={activeStep}
        className={classes.stepper}
        classes={{ dotActive: classes.dotActive }}
      />
    </div>
  );
}

export default SwipeableTextMobileStepper;
