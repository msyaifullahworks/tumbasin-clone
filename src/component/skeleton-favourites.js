import React from "react";
import ContentLoader from "react-content-loader";

const SkeletonFavourites = () => (
  <ContentLoader
    speed={2}
    height={900}
    viewBox="0 0 444 900"
    backgroundColor="lightgrey"
    // foregroundColor="black"
    style={{ width: "100%", maxWidth: 444, marginTop: -100 }}
  >
    <rect x="20" y="750" rx="0" ry="0" width="90" height="102.5" />
    <rect x="134" y="750" rx="0" ry="0" width="290" height="102.5" />
  </ContentLoader>
);

export default SkeletonFavourites;
