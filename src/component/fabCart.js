import React, { useContext } from "react";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { CartContext } from "../context/cart";
import CurrencyFormatter from "../currency-formatter";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      position: "fixed",
      justifyContent: "center",
      bottom: 68,
      zIndex: 9,
      width: "100%",
      left: 0,
      padding: "0 8px",
    },
    paper: {
      width: "100%",
      maxWidth: 420,
      height: 41,
      backgroundColor: "#F15B5D",
    },
    cart: {
      color: "white",
      paddingLeft: 16,
    },
  })
);

function FabCart() {
  const classes = useStyles();
  const { cart, price } = useContext(CartContext);
  return (
    <div className={classes.wrapper}>
      {cart.length > 0 && (
        <div className={classes.root}>
          <Paper className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              className={classes.cart}
            >
              <Grid item xs={6}>
                <div style={{ fontSize: 10 }}>
                  <p style={{ margin: "unset", paddingTop: 4 }}>
                    {cart.length} item | {CurrencyFormatter.format(price)}
                  </p>
                  <div>
                    <p style={{ fontSize: 8 }}>Pasar Bulu Semarang</p>
                  </div>
                </div>
              </Grid>
              <Grid item xs={6} align="right" style={{ paddingRight: 20 }}>
                <ShoppingCartIcon />
              </Grid>
            </Grid>
          </Paper>
        </div>
      )}
    </div>
  );
}

export default FabCart;
