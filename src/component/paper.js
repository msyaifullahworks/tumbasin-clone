import React, { useState, useEffect } from "react";
//importing component
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import ProductCategory from "./product-category";
import SwipeableTextMobileStepper from "./slider";
import SearchBar from "../component/search-bar";
import Header from "../component/header";
import Favourites from "../component/favourite-product";
import FabCart from "./fabCart";

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    boxShadow: "none",
    border: "0.1px solid #dbdbdb",
    minHeight: "100vh",
    borderRadius: 0,
    fontFamily: "Montserrat, sans-serif",
    marginBottom: 50,
  },
  divSlider: {
    display: "flex",
    justifyContent: "center",
  },
}));

function PaperSheet() {
  const classes = useStyles();
  const [onTop, setOnTop] = useState(true);
  // const [loading, setLoading] = useState(true);
  const handleScroll = () => {
    if (window.scrollY < 80) {
      setOnTop(true);
      console.log("scroll");
    } else {
      setOnTop(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  useEffect(() => {
    setTimeout(() => {
      window.scrollTo(0, 0);
    }, 100);
  }, []);

  return (
    <div>
      <Paper className={classes.root}>
        <SearchBar onTop={onTop} />
        <Header onTop={!onTop} />
        <div id="searchBar">
          <ProductCategory />
        </div>
        <div className={classes.divSlider}>
          <SwipeableTextMobileStepper />
        </div>
        <Favourites />
        <FabCart />
      </Paper>
    </div>
  );
}

export default PaperSheet;
