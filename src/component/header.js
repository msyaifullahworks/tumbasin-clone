import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import StorefrontIcon from "@material-ui/icons/Storefront";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import SearchIcon from "@material-ui/icons/Search";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    "& > *": {
      // marginTop: -20,
      width: "100%",
      maxWidth: 444,
      height: 48,
    },
  },
  store: {
    position: "fixed",
    top: 0,
    zIndex: 999,
    display: "flex",
    direction: "row",
    justify: "center",
    alignItems: "center",
    padding: "0 20px",
  },
  storeicon: {
    color: "rgb(135, 202, 254)",
  },
  expand: {
    color: "grey",
  },
  search: {
    color: "grey",
  },
}));

function Header({ onTop }) {
  const classes = useStyles();
  return (
    <Fade in={onTop}>
      <div className={classes.root}>
        <Paper className={classes.store}>
          <Grid item xs={1}>
            <StorefrontIcon className={classes.storeicon} />
          </Grid>
          <Grid item xs={7}>
            <strong>
              <p>Pasar Bulu Semarang</p>
            </strong>
          </Grid>
          <Grid item xs={3}>
            <ExpandMoreIcon className={classes.expand} />
          </Grid>
          <Grid item xs={1}>
            <SearchIcon className={classes.search} />
          </Grid>
        </Paper>
      </div>
    </Fade>
  );
}
export default Header;
