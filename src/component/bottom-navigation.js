import React from "react";
//importing component
import { makeStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import StorefrontIcon from "@material-ui/icons/Storefront";
import ReceiptIcon from "@material-ui/icons/Receipt";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import PersonIcon from "@material-ui/icons/Person";

const useStyles = makeStyles({
  root: {
    width: "100%",
    maxWidth: 444,
    position: "fixed",
    bottom: 0,
    boxShadow: "0px 0px 2px #9e9e9e",
  },
  actionItem: {
    fontSize: 11,
    "&$selected": {
      color: "#F15B5D",
    },
  },
  selected: {},
});

function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root}
    >
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Belanja"
        icon={<StorefrontIcon />}
      />
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Transaksi"
        icon={<ReceiptIcon />}
      />
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Bantuan"
        icon={<LiveHelpIcon />}
      />
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Profile"
        icon={<PersonIcon />}
      />
    </BottomNavigation>
  );
}
export default SimpleBottomNavigation;
