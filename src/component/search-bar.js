import React from "react";
//importing component
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import AppBar from "./app-bar";
import Zoom from "@material-ui/core/Zoom";

const useStyles = makeStyles((theme) => ({
  search: {
    // margin: theme.spacing(1),
    borderRadius: "50px",
    backgroundColor: "white",
    border: "none",
    width: "100%",
    // marginLeft: 20,
    height: 35,
    display: "flex",
    justifyContent: "center",
  },
  paper: {
    top: 0,
    borderRadius: 0,
    backgroundImage: "linear-gradient(to right, #f77062 0%, #fe5196 100%)",
    height: 120,
    width: "100%",
  },
  icon: {
    color: "grey",
    marginLeft: 10,
  },
  searchWrapper: {
    padding: "0 16px",
    paddingTop: 16,
  },
}));

function SearchBar({ onTop }) {
  const classes = useStyles();

  return (
    <Zoom in={onTop}>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <div className={classes.searchWrapper}>
            <InputBase
              className={classes.search}
              id="input-with-icon-adornment"
              placeholder="Search..."
              startAdornment={
                <InputAdornment position="start">
                  <SearchIcon className={classes.icon} />
                </InputAdornment>
              }
            />
          </div>
        </Paper>
        <AppBar />
      </div>
    </Zoom>
  );
}
export default SearchBar;
