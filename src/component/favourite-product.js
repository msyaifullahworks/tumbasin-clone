import React from "react";
//importing component
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ButtonAdd from "../component/button-add";
import CurrencyFormatter from "../../src/currency-formatter";
import { useQuery } from "react-query";
import SkeletonFavourites from "./skeleton-favourites";
import CircularProgress from "@material-ui/core/CircularProgress";

const getFavourites = async () => {
  const URL = `https://api.tumbasin.id/t/v1/products?vendor=1003&featured=true`;
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Fetching Error");
  }
  return await response.json();
};

const useStyles = makeStyles((theme) => ({
  grid: {
    padding: "0 16px",
    marginTop: 8,
  },
  viewAll: {
    color: "#F15b5d",
  },
  buy: {
    display: "flex",
    justifyContent: "space-between",
  },
  button: {
    backgroundColor: "#F15b5d",
    color: "white",
    border: "none",
    borderRadius: 4,
    width: 80,
    height: 30,
    fontSize: "10px",
    fontFamily: "Montserrat, sans-serrif",
  },
  image: {
    width: "100%",
  },
  product: {
    fontSize: 12,
    paddingLeft: 16,
  },
  price: {
    paddingLeft: 16,
    display: "flex",
  },
  textPrice: {
    color: "#F15b5d",
    fontWeight: "bold",
    fontSize: 12,
  },
  weight: {
    color: "#C7c7c9",
    marginLeft: 5,
    marginTop: 4,
    alignItems: "center",
    fontSize: 10,
  },
  card: {
    borderBottom: "1px solid lightgrey",
    padding: "0 16px",
    paddingTop: 16,
  },
  products: {
    marginTop: -8,
    [theme.breakpoints.up("xs")]: {
      paddingBottom: 64,
    },
  },
}));

function Favourites() {
  const classes = useStyles();
  const { data, isLoading, isError, isSuccess } = useQuery(
    "favourite-product",
    () => getFavourites()
  );
  return (
    <div>
      {isLoading && <SkeletonFavourites />}
      {isError && <p>There was an error processing your request</p>}
      <Grid
        className={classes.grid}
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={8}>
          <strong>
            <p>Produk Terlaris</p>
          </strong>
        </Grid>
        <Grid className={classes.viewAll} item xs={4} align="right">
          <strong>
            <p>Lihat Semua</p>
          </strong>
        </Grid>
      </Grid>
      <div className={classes.products}>
        {isSuccess &&
          data?.slice(0, 5).map((item) => (
            <Grid
              className={classes.card}
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={3}>
                <img
                  className={classes.image}
                  src={item.images[0].src}
                  alt=""
                />
              </Grid>
              <Grid item xs={9}>
                <div className={classes.product}>
                  <p className={classes.category}>{item.name}</p>
                </div>
                <div className={classes.buy}>
                  <div className={classes.price}>
                    <Typography className={classes.textPrice} variant="body">
                      {CurrencyFormatter.format(item.price)}
                    </Typography>
                    <Typography className={classes.weight} variant="body">
                      / {item.meta_data[0].value}
                    </Typography>
                  </div>
                  <ButtonAdd data={item} />
                </div>
              </Grid>
            </Grid>
          ))}
      </div>
    </div>
  );
}

export default Favourites;
