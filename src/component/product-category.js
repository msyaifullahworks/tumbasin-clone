import React from "react";
//importing component
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { useQuery } from "react-query";
import SkeletonCategories from "./skeleton-categories";

const getCategories = async () => {
  const URL = `https://api.tumbasin.id/t/v1/products/categories`;
  const response = await fetch(URL);
  return await response.json();
};

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    // justifyContent: "center",
    "& > *": {
      width: 45,
      height: 45,
      fontSize: 10,
    },
  },
  category: {
    textAlign: "center",
    fontWeight: 400,
    marginTop: 3,
    whiteSpace: "nowrap",
    color: "#707585",
  },
  image: {
    textAlign: "center",
    width: 40,
  },
  paper: {
    textAlign: "center",
    backgroundColor: "#FDF4FC",
    padding: 3,
  },
  productcat: {
    marginTop: 16,
  },
  varian: {
    marginTop: -20,
  },
  jenis: {
    marginTop: 16,
    paddingLeft: 16,
  },
  paperWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "20%",
    padding: 16,
    marginBottom: 32,
  },
}));

function ProductCategory(props) {
  const { data, isLoading, isFetching, isError, isSuccess } = useQuery(
    "product-category",
    getCategories
  );
  const classes = useStyles();
  return (
    <div className={classes.productcat}>
      {isLoading && <SkeletonCategories />}
      {isError && <p>There was an error processing your request</p>}
      <p className={classes.jenis}>
        <strong>Telusuri jenis produk</strong>
      </p>
      <div className={classes.varian}>
        <div className={classes.root}>
          {isSuccess &&
            data.map((item) => (
              <div className={classes.paperWrapper}>
                <Paper className={classes.paper}>
                  <img className={classes.image} src={item.image.src} alt="" />
                </Paper>
                <p className={classes.category}>{item.name}</p>
              </div>
            ))}
          {/* <Paper className={classes.paper} style={{ visibility: "hidden" }} /> */}
        </div>
      </div>
    </div>
  );
}
export default ProductCategory;
