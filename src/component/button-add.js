import React, { useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Typography } from "@material-ui/core";
import { CartContext } from "../context/cart";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      height: 30,
    },
  },
  text: {
    fontSize: 10,
    fontFamily: "Montserrat, sans-serrif",
  },
}));

function ButtonAdd({ data }) {
  const { addCart, cart, increaseCart, decreaseCart } = useContext(CartContext);
  const classes = useStyles();
  const [quantity, setQuantity] = useState(0);
  const filteredCart = cart.filter((item) => item.id === data.id);

  return (
    <div className={classes.root}>
      {filteredCart[0]?.total > 0 ? (
        <div style={{ display: "flex" }}>
          <button
            style={{
              width: 25,
              height: 25,
              borderRadius: 4,
              boxShadow: "unset",
              backgroundColor: "white",
              border: "1px solid lightgrey",
            }}
            onClick={() => decreaseCart(data)}
          >
            -
          </button>
          <Typography
            style={{
              fontFamily: "Montserrat, sans-serif",
              fontSize: 14,
              display: "flex",
              alignItems: "center",
              fontWeight: "bold",
              padding: "0 10px",
            }}
          >
            {filteredCart[0]?.total}
          </Typography>
          <button
            style={{
              width: 25,
              height: 25,
              borderRadius: 4,
              boxShadow: "unset",
              backgroundColor: "rgb(241, 91, 93)",
              border: "unset",
              color: "white",
            }}
            onClick={() => increaseCart(data)}
          >
            +
          </button>
        </div>
      ) : (
        <Button
          onClick={() => addCart(data)}
          variant="contained"
          style={{
            backgroundColor: "rgb(241, 91, 93)",
            color: "white",
            width: 80,
            boxShadow: "unset",
          }}
        >
          <p className={classes.text}>TAMBAHKAN</p>
        </Button>
      )}
    </div>
  );
}
export default ButtonAdd;
