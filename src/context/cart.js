import React, { createContext, useState, useEffect } from "react";

export const CartContext = createContext();

const CartContextProvider = ({ children }) => {
  const [cart, setCart] = useState([]);
  const [price, setPrice] = useState(0);
  const [cartUpdated, setCartUpdated] = useState(true);

  useEffect(() => {
    const carts = JSON.parse(localStorage.getItem("cart"));
    const prices = JSON.parse(localStorage.getItem("price"));
    if (carts) {
      setCart(carts);
      setPrice(prices);
    }
  }, []);

  const calculateTotal = (list) => {
    if (list.length > 0) {
      const sum = (item) => item.reduce((x, y) => x + y);
      let total = sum(list.map((product) => Number(product.totalPrice)));
      setPrice(total);

      localStorage.setItem("price", total);
      setCartUpdated(!cartUpdated);
    } else {
      setPrice(0);
      localStorage.setItem("price", 0);
      setCartUpdated(!cartUpdated);
    }
  };

  const addCart = (item) => {
    const cartData = [...cart, { ...item, total: 1, totalPrice: item.price }];
    setCart(cartData);
    calculateTotal(cartData);
    localStorage.setItem("cart", JSON.stringify(cartData));
  };

  const increaseCart = (item) => {
    const objIndex = cart.findIndex((select) => {
      return select.id === item.id;
    });
    let cartData = cart;
    cartData[objIndex].total += 1;
    cartData[objIndex].totalPrice =
      cartData[objIndex].total * cartData[objIndex].price;
    setCart(cartData);
    calculateTotal(cartData);
    localStorage.setItem("cart", JSON.stringify(cartData));
  };

  const decreaseCart = (item) => {
    const objIndex = cart.findIndex((select) => {
      return select.id === item.id;
    });
    let cartData = cart;
    if (cartData[objIndex].total > 1) {
      cartData[objIndex].total -= 1;
      cartData[objIndex].totalPrice =
        cartData[objIndex].total * cartData[objIndex].price;
      setCart(cartData);
      calculateTotal(cartData);
      localStorage.setItem("cart", JSON.stringify(cartData));
    } else {
      const newCart = cart.filter((obj) => {
        return obj.id !== item.id;
      });
      setCart(newCart);
      calculateTotal(newCart);
      localStorage.setItem("cart", JSON.stringify(newCart));
    }
  };

  return (
    <CartContext.Provider
      value={{
        cart,
        addCart,
        calculateTotal,
        increaseCart,
        decreaseCart,
        price,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
