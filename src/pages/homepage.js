import React from "react";
//importing component
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import SimpleBottomNavigation from "../component/bottom-navigation";
import { makeStyles } from "@material-ui/core/styles";
import PaperSheet from "../component/paper";

const useStyles = makeStyles({
  root: {
    display: "flex",
    justifyContent: "center",
    fontFamily: "Montserrat, sans-serrif",
    padding: 0,
  },
});

function Homepage() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="xs" className={classes.root}>
        <PaperSheet />
        <SimpleBottomNavigation />
      </Container>
    </React.Fragment>
  );
}
export default Homepage;
